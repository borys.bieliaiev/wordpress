#!/bin/bash

echo 'Version Wordpress:'
read WORDPRESS_VERSION

echo 'Service Name (i.e. wordpress-5-4-2)':
read SVC_NAME

mkdir wordpress-${WORDPRESS_VERSION}
sleep 1

# SET DEFAULT NAMESPACE
kubectl config set-context --current --namespace=wordpress

#Deploy MYSQL PVC

cp template/mysql-pvc.yaml wordpress-${WORDPRESS_VERSION}/mysql-pvc.yaml
sed -i "s/<VERSION>/${WORDPRESS_VERSION}/g" wordpress-${WORDPRESS_VERSION}/mysql-pvc.yaml
kubectl create -f wordpress-${WORDPRESS_VERSION}/mysql-pvc.yaml

#MYSQL CONFIG MAP 
kubectl create configmap wordpress-database-data-${WORDPRESS_VERSION} --from-literal=user=wordpress --from-literal=database=wordpress-${WORDPRESS_VERSION}

#MYSQL DEPLOY
cp template/mysql-deploy.yaml wordpress-${WORDPRESS_VERSION}/mysql-deploy.yaml
sed -i "s/<VERSION>/${WORDPRESS_VERSION}/g" wordpress-${WORDPRESS_VERSION}/mysql-deploy.yaml
kubectl create -f wordpress-${WORDPRESS_VERSION}/mysql-deploy.yaml
kubectl expose deployment mysql-${WORDPRESS_VERSION} --name mysql-${SVC_NAME} --port 3306

#Deploy WORDPRESS PVC
cp template/wordpress-pvc.yaml wordpress-${WORDPRESS_VERSION}/wordpress-pvc.yaml
sed -i "s/<VERSION>/${WORDPRESS_VERSION}/g" wordpress-${WORDPRESS_VERSION}/wordpress-pvc.yaml
sleep 1
kubectl create -f wordpress-${WORDPRESS_VERSION}/wordpress-pvc.yaml

#WORDPRESS DEPLOYMENT 

cp template/wordpress-deploy.yaml wordpress-${WORDPRESS_VERSION}/wordpress-deploy.yaml
sed -i "s/<VERSION>/${WORDPRESS_VERSION}/g" wordpress-${WORDPRESS_VERSION}/wordpress-deploy.yaml
sed -i "s/<SVC_NAME>/${SVC_NAME}/g" wordpress-${WORDPRESS_VERSION}/wordpress-deploy.yaml
kubectl create -f wordpress-${WORDPRESS_VERSION}/wordpress-deploy.yaml
kubectl expose deployment wordpress-${WORDPRESS_VERSION} --name wordpress-${SVC_NAME} --type=LoadBalancer 
